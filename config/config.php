<?php
    // Каталоги сайта
    define('SITE_DIR', __DIR__ . '/../');               // Выход на один уровень вверх в структуре каталогов сайта
    define('CONFIG_DIR', SITE_DIR . 'config/');         // Конфигурация
    define('DATA_DIR', SITE_DIR . 'data/');             // Данные
    define('ENGINE_DIR', SITE_DIR . 'engine/');         // Функционал
    define('WWW_DIR', SITE_DIR . 'public/');            // Каталог, доступный посетителям сайта
    define('TEMPLATES_DIR', SITE_DIR . 'templates/');   // Шаблоны
    define('IMG_DIR', 'img/');                          // Изображения
    
    // Константы соединения с б/д
    define('DB_HOST', 'ls');                            // Доменное имя сервера
    define('DB_USER', 'jetsaus');                       // Имя пользователя
    define('DB_PASS', 'opdf117!');                      // Пароль
    define('DB_NAME', 'gb');                       // Имя БД
    
    require_once ENGINE_DIR . 'db.php';                 // Функции работы с БД
    require_once ENGINE_DIR . 'reviews.php';            // Функции работы с комментариями
