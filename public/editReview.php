<?php
    require_once __DIR__ . '/../config/config.php';
    $id = isset($_GET['id']) ? $_GET['id'] : false;
    if (!$id) {
        echo 'id не передан';
        exit();
    }
    $review = getReview($id);
    
    // Редактируем отзыв
    $author = $_POST['author'] ?? $review['author'];
    $text = $_POST['text'] ?? $review['text'];
    // Проверка, редактировался ли отзыв
    if ($author !== $review['author'] || $text !== $review['text']) {
        if ($author && $text) {
            if (updateReview($id, $author, $text)) {
                echo 'Комментарий изменен';
            } else {
                echo 'Произошла ошибка';
            }
        } elseif ($author || $text) {
            echo 'Форма не заполнена';
        }
    }
    echo '<hr>';
    $reviews = getReviews();
    echo '<div class=\"reviews\">';
    foreach ($reviews as $review) {
        echo '<div class=\"review\">';
        echo $review['author'] . ': ' . $review['text'];
        echo " <a href=\"editReview.php?id=" . $review['id'] . "\">Редактировать</a>";
        echo " <a href=\"deleteReview.php?id=" . $review['id'] . "\">Удалить</a>";
        echo '<div>';
    }
    echo '</div>';

?>
<h4>Редактировать отзыв: <?= $review['id'] ?></h4>
<form method="POST">
    <span>Имя: </span><input type="text" name="author" value="<?= $author ?>"><br>
    <span>Комментарий: </span><textarea name="text"><?= $text ?></textarea><br>
    <input type="submit" value="Отправить">
</form>
<a href="index.php">На главную</a>